﻿Public Class RobotRoundReport
    Public Property RobotRound As RobotRound
    Public Property parentForm As RobotRoundForm

    Public Sub New(form As RobotRoundForm, newRobotRound As RobotRound)
        ' Load RobotRound and all it's values into the relative textboxes
        ' and checkboxes.


        ' This call is required by the designer.
        InitializeComponent()

        ' Store parent form, as it is used to execute Reset Round on form close
        Me.parentForm = form

        Me.RobotRound = newRobotRound

        Me.RoundNumberLabel.Text = Me.RobotRound.RoundId
        Me.RobotNumberLabel.Text = Me.RobotRound.RobotId
        Me.GoalsScoredLabel.Text = Me.RobotRound.GoalsScored

        ' Load Auto mode images - a tick or a cross
        LoadPicture(Me.AutoReachedObstaclePictureBox, Me.RobotRound.AutoReachedObstacle)
        LoadPicture(Me.AutoCrossedObstaclePictureBox, Me.RobotRound.AutoCrossedObstacle)
        LoadPicture(Me.AutoScoredLowGoalPictureBox, Me.RobotRound.AutoScoredLowGoal)
        LoadPicture(Me.AutoScoredHighGoalPictureBox, Me.RobotRound.AutoScoredHighGoal)

        ' Load Teleop mode images - a tick or a cross
        ' The obstacles passed in Teleop mode
        LoadPicture(Me.TeleopLowBarPictureBox, Me.RobotRound.TeleopLowBar)
        LoadPicture(Me.TeleopPortcullisPictureBox, Me.RobotRound.TeleopPortcullis)
        LoadPicture(Me.TeleopChevalDeFrisPictureBox, Me.RobotRound.TeleopChevalDeFris)
        LoadPicture(Me.TeleopMoatPictureBox, Me.RobotRound.TeleopMoat)
        LoadPicture(Me.TeleopRampartsPictureBox, Me.RobotRound.TeleopRamparts)
        LoadPicture(Me.TeleopDrawbridgePictureBox, Me.RobotRound.TeleopDrawbridge)
        LoadPicture(Me.TeleopSallyPortPictureBox, Me.RobotRound.TeleopSallyPort)
        LoadPicture(Me.TeleopRockWallPictureBox, Me.RobotRound.TeleopRockWall)
        LoadPicture(Me.TeleopRoughTerrainPictureBox, Me.RobotRound.TeleopRoughTerrain)

        ' The robot abilities demonstrated in Teleop mode
        LoadPicture(Me.TeleopPickedUpBallsPictureBox, Me.RobotRound.TeleopPickUpBalls)
        LoadPicture(Me.TeleopScoredLowGoalsPictureBox, Me.RobotRound.TeleopScoreLowGoals)
        LoadPicture(Me.TeleopScoredHighGoalsPictureBox, Me.RobotRound.TeleopScoreHighGoals)
        LoadPicture(Me.TeleopClimbedWallsPictureBox, Me.RobotRound.TeleopClimbWall)

    End Sub

    Private Sub CloseButton_Click(sender As Object, e As EventArgs) Handles CloseButton.Click
        ' Close this form
        Me.Close()
    End Sub

    Private Sub LoadPicture(pictureBox As PictureBox, checked As Boolean)
        ' Determines which image should be displayed for the specified 
        ' PictureBox

        ' Image reference
        '        We Love Solo, (2014), Inset On Off Toggle Switch Set, 
        '        viewed 24 March 2016 <https: 
        '        //www.welovesolo.com/inset-on-off-toggle-switch-set/>

        If (checked) Then
            pictureBox.Image = My.Resources.option_checked
        Else
            pictureBox.Image = My.Resources.option_unchecked
        End If
    End Sub

    Private Sub CloseAndResetRoundButton_Click(sender As Object, e As EventArgs) Handles CloseAndResetRoundButton.Click
        ' Reset the round and close this form
        Me.parentForm.ResetRound()
        Me.Close()
    End Sub
End Class