﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RobotRoundReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.RoundNumberLabel = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.RobotNumberLabel = New System.Windows.Forms.Label()
        Me.CloseButton = New System.Windows.Forms.Button()
        Me.AutoModeGroupBox = New System.Windows.Forms.GroupBox()
        Me.AutoScoredHighGoalPictureBox = New System.Windows.Forms.PictureBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.AutoScoredLowGoalPictureBox = New System.Windows.Forms.PictureBox()
        Me.AutoCrossedObstaclePictureBox = New System.Windows.Forms.PictureBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.AutoReachedObstaclePictureBox = New System.Windows.Forms.PictureBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TeleopGroupBox = New System.Windows.Forms.GroupBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.TeleopRoughTerrainPictureBox = New System.Windows.Forms.PictureBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.TeleopClimbedWallsPictureBox = New System.Windows.Forms.PictureBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TeleopScoredHighGoalsPictureBox = New System.Windows.Forms.PictureBox()
        Me.TeleopScoredLowGoalsPictureBox = New System.Windows.Forms.PictureBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TeleopPickedUpBallsPictureBox = New System.Windows.Forms.PictureBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TeleopRockWallPictureBox = New System.Windows.Forms.PictureBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TeleopSallyPortPictureBox = New System.Windows.Forms.PictureBox()
        Me.TeleopDrawbridgePictureBox = New System.Windows.Forms.PictureBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TeleopRampartsPictureBox = New System.Windows.Forms.PictureBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TeleopMoatPictureBox = New System.Windows.Forms.PictureBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TeleopChevalDeFrisPictureBox = New System.Windows.Forms.PictureBox()
        Me.TeleopPortcullisPictureBox = New System.Windows.Forms.PictureBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TeleopLowBarPictureBox = New System.Windows.Forms.PictureBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.GoalsScoredLabel = New System.Windows.Forms.Label()
        Me.CloseAndResetRoundButton = New System.Windows.Forms.Button()
        Me.AutoModeGroupBox.SuspendLayout()
        CType(Me.AutoScoredHighGoalPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AutoScoredLowGoalPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AutoCrossedObstaclePictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AutoReachedObstaclePictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TeleopGroupBox.SuspendLayout()
        CType(Me.TeleopRoughTerrainPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeleopClimbedWallsPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeleopScoredHighGoalsPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeleopScoredLowGoalsPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeleopPickedUpBallsPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeleopRockWallPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeleopSallyPortPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeleopDrawbridgePictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeleopRampartsPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeleopMoatPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeleopChevalDeFrisPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeleopPortcullisPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeleopLowBarPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(13, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 18)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Round #:"
        '
        'RoundNumberLabel
        '
        Me.RoundNumberLabel.AutoSize = True
        Me.RoundNumberLabel.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RoundNumberLabel.Location = New System.Drawing.Point(93, 23)
        Me.RoundNumberLabel.Name = "RoundNumberLabel"
        Me.RoundNumberLabel.Size = New System.Drawing.Size(62, 18)
        Me.RoundNumberLabel.TabIndex = 1
        Me.RoundNumberLabel.Text = "Label2"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(162, 23)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(82, 18)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Robot #:"
        '
        'RobotNumberLabel
        '
        Me.RobotNumberLabel.AutoSize = True
        Me.RobotNumberLabel.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RobotNumberLabel.Location = New System.Drawing.Point(240, 23)
        Me.RobotNumberLabel.Name = "RobotNumberLabel"
        Me.RobotNumberLabel.Size = New System.Drawing.Size(62, 18)
        Me.RobotNumberLabel.TabIndex = 3
        Me.RobotNumberLabel.Text = "Label3"
        '
        'CloseButton
        '
        Me.CloseButton.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CloseButton.Location = New System.Drawing.Point(366, 448)
        Me.CloseButton.Name = "CloseButton"
        Me.CloseButton.Size = New System.Drawing.Size(75, 23)
        Me.CloseButton.TabIndex = 4
        Me.CloseButton.Text = "Close"
        Me.CloseButton.UseVisualStyleBackColor = True
        '
        'AutoModeGroupBox
        '
        Me.AutoModeGroupBox.Controls.Add(Me.AutoScoredHighGoalPictureBox)
        Me.AutoModeGroupBox.Controls.Add(Me.Label6)
        Me.AutoModeGroupBox.Controls.Add(Me.AutoScoredLowGoalPictureBox)
        Me.AutoModeGroupBox.Controls.Add(Me.AutoCrossedObstaclePictureBox)
        Me.AutoModeGroupBox.Controls.Add(Me.Label5)
        Me.AutoModeGroupBox.Controls.Add(Me.Label4)
        Me.AutoModeGroupBox.Controls.Add(Me.AutoReachedObstaclePictureBox)
        Me.AutoModeGroupBox.Controls.Add(Me.Label3)
        Me.AutoModeGroupBox.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AutoModeGroupBox.Location = New System.Drawing.Point(12, 54)
        Me.AutoModeGroupBox.Name = "AutoModeGroupBox"
        Me.AutoModeGroupBox.Size = New System.Drawing.Size(205, 183)
        Me.AutoModeGroupBox.TabIndex = 9
        Me.AutoModeGroupBox.TabStop = False
        Me.AutoModeGroupBox.Text = "Autonomous Mode"
        '
        'AutoScoredHighGoalPictureBox
        '
        Me.AutoScoredHighGoalPictureBox.Location = New System.Drawing.Point(16, 135)
        Me.AutoScoredHighGoalPictureBox.Name = "AutoScoredHighGoalPictureBox"
        Me.AutoScoredHighGoalPictureBox.Size = New System.Drawing.Size(30, 30)
        Me.AutoScoredHighGoalPictureBox.TabIndex = 14
        Me.AutoScoredHighGoalPictureBox.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(50, 138)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(118, 16)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Scored high goal"
        '
        'AutoScoredLowGoalPictureBox
        '
        Me.AutoScoredLowGoalPictureBox.Location = New System.Drawing.Point(16, 100)
        Me.AutoScoredLowGoalPictureBox.Name = "AutoScoredLowGoalPictureBox"
        Me.AutoScoredLowGoalPictureBox.Size = New System.Drawing.Size(30, 30)
        Me.AutoScoredLowGoalPictureBox.TabIndex = 12
        Me.AutoScoredLowGoalPictureBox.TabStop = False
        '
        'AutoCrossedObstaclePictureBox
        '
        Me.AutoCrossedObstaclePictureBox.Location = New System.Drawing.Point(16, 65)
        Me.AutoCrossedObstaclePictureBox.Name = "AutoCrossedObstaclePictureBox"
        Me.AutoCrossedObstaclePictureBox.Size = New System.Drawing.Size(30, 30)
        Me.AutoCrossedObstaclePictureBox.TabIndex = 12
        Me.AutoCrossedObstaclePictureBox.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(50, 103)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(113, 16)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Scored low goal"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(50, 68)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(121, 16)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Crossed obstacle"
        '
        'AutoReachedObstaclePictureBox
        '
        Me.AutoReachedObstaclePictureBox.Location = New System.Drawing.Point(16, 30)
        Me.AutoReachedObstaclePictureBox.Name = "AutoReachedObstaclePictureBox"
        Me.AutoReachedObstaclePictureBox.Size = New System.Drawing.Size(30, 30)
        Me.AutoReachedObstaclePictureBox.TabIndex = 10
        Me.AutoReachedObstaclePictureBox.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(50, 33)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(125, 16)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Reached obstacle"
        '
        'TeleopGroupBox
        '
        Me.TeleopGroupBox.Controls.Add(Me.Label21)
        Me.TeleopGroupBox.Controls.Add(Me.Label20)
        Me.TeleopGroupBox.Controls.Add(Me.TeleopRoughTerrainPictureBox)
        Me.TeleopGroupBox.Controls.Add(Me.Label19)
        Me.TeleopGroupBox.Controls.Add(Me.TeleopClimbedWallsPictureBox)
        Me.TeleopGroupBox.Controls.Add(Me.Label15)
        Me.TeleopGroupBox.Controls.Add(Me.TeleopScoredHighGoalsPictureBox)
        Me.TeleopGroupBox.Controls.Add(Me.TeleopScoredLowGoalsPictureBox)
        Me.TeleopGroupBox.Controls.Add(Me.Label16)
        Me.TeleopGroupBox.Controls.Add(Me.Label17)
        Me.TeleopGroupBox.Controls.Add(Me.TeleopPickedUpBallsPictureBox)
        Me.TeleopGroupBox.Controls.Add(Me.Label18)
        Me.TeleopGroupBox.Controls.Add(Me.TeleopRockWallPictureBox)
        Me.TeleopGroupBox.Controls.Add(Me.Label11)
        Me.TeleopGroupBox.Controls.Add(Me.TeleopSallyPortPictureBox)
        Me.TeleopGroupBox.Controls.Add(Me.TeleopDrawbridgePictureBox)
        Me.TeleopGroupBox.Controls.Add(Me.Label12)
        Me.TeleopGroupBox.Controls.Add(Me.Label13)
        Me.TeleopGroupBox.Controls.Add(Me.TeleopRampartsPictureBox)
        Me.TeleopGroupBox.Controls.Add(Me.Label14)
        Me.TeleopGroupBox.Controls.Add(Me.TeleopMoatPictureBox)
        Me.TeleopGroupBox.Controls.Add(Me.Label7)
        Me.TeleopGroupBox.Controls.Add(Me.TeleopChevalDeFrisPictureBox)
        Me.TeleopGroupBox.Controls.Add(Me.TeleopPortcullisPictureBox)
        Me.TeleopGroupBox.Controls.Add(Me.Label8)
        Me.TeleopGroupBox.Controls.Add(Me.Label9)
        Me.TeleopGroupBox.Controls.Add(Me.TeleopLowBarPictureBox)
        Me.TeleopGroupBox.Controls.Add(Me.Label10)
        Me.TeleopGroupBox.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TeleopGroupBox.Location = New System.Drawing.Point(236, 54)
        Me.TeleopGroupBox.Name = "TeleopGroupBox"
        Me.TeleopGroupBox.Size = New System.Drawing.Size(374, 388)
        Me.TeleopGroupBox.TabIndex = 10
        Me.TeleopGroupBox.TabStop = False
        Me.TeleopGroupBox.Text = "Teleop Mode"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(187, 30)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(112, 16)
        Me.Label21.TabIndex = 48
        Me.Label21.Text = "Robot Abilities"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(15, 30)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(116, 16)
        Me.Label20.TabIndex = 47
        Me.Label20.Text = "Past Obstacles"
        '
        'TeleopRoughTerrainPictureBox
        '
        Me.TeleopRoughTerrainPictureBox.Location = New System.Drawing.Point(16, 341)
        Me.TeleopRoughTerrainPictureBox.Name = "TeleopRoughTerrainPictureBox"
        Me.TeleopRoughTerrainPictureBox.Size = New System.Drawing.Size(30, 30)
        Me.TeleopRoughTerrainPictureBox.TabIndex = 46
        Me.TeleopRoughTerrainPictureBox.TabStop = False
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(50, 344)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(99, 16)
        Me.Label19.TabIndex = 45
        Me.Label19.Text = "Rough Terrain"
        '
        'TeleopClimbedWallsPictureBox
        '
        Me.TeleopClimbedWallsPictureBox.Location = New System.Drawing.Point(187, 164)
        Me.TeleopClimbedWallsPictureBox.Name = "TeleopClimbedWallsPictureBox"
        Me.TeleopClimbedWallsPictureBox.Size = New System.Drawing.Size(30, 30)
        Me.TeleopClimbedWallsPictureBox.TabIndex = 38
        Me.TeleopClimbedWallsPictureBox.TabStop = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(221, 167)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(88, 16)
        Me.Label15.TabIndex = 37
        Me.Label15.Text = "Climbed wall"
        '
        'TeleopScoredHighGoalsPictureBox
        '
        Me.TeleopScoredHighGoalsPictureBox.Location = New System.Drawing.Point(187, 129)
        Me.TeleopScoredHighGoalsPictureBox.Name = "TeleopScoredHighGoalsPictureBox"
        Me.TeleopScoredHighGoalsPictureBox.Size = New System.Drawing.Size(30, 30)
        Me.TeleopScoredHighGoalsPictureBox.TabIndex = 35
        Me.TeleopScoredHighGoalsPictureBox.TabStop = False
        '
        'TeleopScoredLowGoalsPictureBox
        '
        Me.TeleopScoredLowGoalsPictureBox.Location = New System.Drawing.Point(187, 94)
        Me.TeleopScoredLowGoalsPictureBox.Name = "TeleopScoredLowGoalsPictureBox"
        Me.TeleopScoredLowGoalsPictureBox.Size = New System.Drawing.Size(30, 30)
        Me.TeleopScoredLowGoalsPictureBox.TabIndex = 36
        Me.TeleopScoredLowGoalsPictureBox.TabStop = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(221, 132)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(125, 16)
        Me.Label16.TabIndex = 33
        Me.Label16.Text = "Scored high goals"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(221, 97)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(120, 16)
        Me.Label17.TabIndex = 34
        Me.Label17.Text = "Scored low goals"
        '
        'TeleopPickedUpBallsPictureBox
        '
        Me.TeleopPickedUpBallsPictureBox.Location = New System.Drawing.Point(187, 59)
        Me.TeleopPickedUpBallsPictureBox.Name = "TeleopPickedUpBallsPictureBox"
        Me.TeleopPickedUpBallsPictureBox.Size = New System.Drawing.Size(30, 30)
        Me.TeleopPickedUpBallsPictureBox.TabIndex = 32
        Me.TeleopPickedUpBallsPictureBox.TabStop = False
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(221, 62)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(105, 16)
        Me.Label18.TabIndex = 31
        Me.Label18.Text = "Picked up balls"
        '
        'TeleopRockWallPictureBox
        '
        Me.TeleopRockWallPictureBox.Location = New System.Drawing.Point(16, 304)
        Me.TeleopRockWallPictureBox.Name = "TeleopRockWallPictureBox"
        Me.TeleopRockWallPictureBox.Size = New System.Drawing.Size(30, 30)
        Me.TeleopRockWallPictureBox.TabIndex = 30
        Me.TeleopRockWallPictureBox.TabStop = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(50, 307)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(71, 16)
        Me.Label11.TabIndex = 29
        Me.Label11.Text = "Rock Wall"
        '
        'TeleopSallyPortPictureBox
        '
        Me.TeleopSallyPortPictureBox.Location = New System.Drawing.Point(16, 269)
        Me.TeleopSallyPortPictureBox.Name = "TeleopSallyPortPictureBox"
        Me.TeleopSallyPortPictureBox.Size = New System.Drawing.Size(30, 30)
        Me.TeleopSallyPortPictureBox.TabIndex = 27
        Me.TeleopSallyPortPictureBox.TabStop = False
        '
        'TeleopDrawbridgePictureBox
        '
        Me.TeleopDrawbridgePictureBox.Location = New System.Drawing.Point(16, 234)
        Me.TeleopDrawbridgePictureBox.Name = "TeleopDrawbridgePictureBox"
        Me.TeleopDrawbridgePictureBox.Size = New System.Drawing.Size(30, 30)
        Me.TeleopDrawbridgePictureBox.TabIndex = 28
        Me.TeleopDrawbridgePictureBox.TabStop = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(50, 272)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(71, 16)
        Me.Label12.TabIndex = 25
        Me.Label12.Text = "Sally Port"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(50, 237)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(81, 16)
        Me.Label13.TabIndex = 26
        Me.Label13.Text = "Drawbridge"
        '
        'TeleopRampartsPictureBox
        '
        Me.TeleopRampartsPictureBox.Location = New System.Drawing.Point(16, 199)
        Me.TeleopRampartsPictureBox.Name = "TeleopRampartsPictureBox"
        Me.TeleopRampartsPictureBox.Size = New System.Drawing.Size(30, 30)
        Me.TeleopRampartsPictureBox.TabIndex = 24
        Me.TeleopRampartsPictureBox.TabStop = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(50, 202)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(69, 16)
        Me.Label14.TabIndex = 23
        Me.Label14.Text = "Ramparts"
        '
        'TeleopMoatPictureBox
        '
        Me.TeleopMoatPictureBox.Location = New System.Drawing.Point(16, 164)
        Me.TeleopMoatPictureBox.Name = "TeleopMoatPictureBox"
        Me.TeleopMoatPictureBox.Size = New System.Drawing.Size(30, 30)
        Me.TeleopMoatPictureBox.TabIndex = 22
        Me.TeleopMoatPictureBox.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(50, 167)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(41, 16)
        Me.Label7.TabIndex = 21
        Me.Label7.Text = "Moat"
        '
        'TeleopChevalDeFrisPictureBox
        '
        Me.TeleopChevalDeFrisPictureBox.Location = New System.Drawing.Point(16, 129)
        Me.TeleopChevalDeFrisPictureBox.Name = "TeleopChevalDeFrisPictureBox"
        Me.TeleopChevalDeFrisPictureBox.Size = New System.Drawing.Size(30, 30)
        Me.TeleopChevalDeFrisPictureBox.TabIndex = 19
        Me.TeleopChevalDeFrisPictureBox.TabStop = False
        '
        'TeleopPortcullisPictureBox
        '
        Me.TeleopPortcullisPictureBox.Location = New System.Drawing.Point(16, 94)
        Me.TeleopPortcullisPictureBox.Name = "TeleopPortcullisPictureBox"
        Me.TeleopPortcullisPictureBox.Size = New System.Drawing.Size(30, 30)
        Me.TeleopPortcullisPictureBox.TabIndex = 20
        Me.TeleopPortcullisPictureBox.TabStop = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(50, 132)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(101, 16)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Cheval de Fris"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(50, 97)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(67, 16)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "Portcullis"
        '
        'TeleopLowBarPictureBox
        '
        Me.TeleopLowBarPictureBox.Location = New System.Drawing.Point(16, 59)
        Me.TeleopLowBarPictureBox.Name = "TeleopLowBarPictureBox"
        Me.TeleopLowBarPictureBox.Size = New System.Drawing.Size(30, 30)
        Me.TeleopLowBarPictureBox.TabIndex = 16
        Me.TeleopLowBarPictureBox.TabStop = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(50, 62)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(60, 16)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "Low bar"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(417, 23)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(121, 18)
        Me.Label22.TabIndex = 11
        Me.Label22.Text = "Goals Scored:"
        '
        'GoalsScoredLabel
        '
        Me.GoalsScoredLabel.AutoSize = True
        Me.GoalsScoredLabel.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GoalsScoredLabel.Location = New System.Drawing.Point(534, 23)
        Me.GoalsScoredLabel.Name = "GoalsScoredLabel"
        Me.GoalsScoredLabel.Size = New System.Drawing.Size(72, 18)
        Me.GoalsScoredLabel.TabIndex = 12
        Me.GoalsScoredLabel.Text = "Label23"
        '
        'CloseAndResetRoundButton
        '
        Me.CloseAndResetRoundButton.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CloseAndResetRoundButton.Location = New System.Drawing.Point(453, 448)
        Me.CloseAndResetRoundButton.Name = "CloseAndResetRoundButton"
        Me.CloseAndResetRoundButton.Size = New System.Drawing.Size(157, 23)
        Me.CloseAndResetRoundButton.TabIndex = 13
        Me.CloseAndResetRoundButton.Text = "Close && Reset Round"
        Me.CloseAndResetRoundButton.UseVisualStyleBackColor = True
        '
        'RobotRoundReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Gainsboro
        Me.ClientSize = New System.Drawing.Size(616, 480)
        Me.Controls.Add(Me.CloseAndResetRoundButton)
        Me.Controls.Add(Me.GoalsScoredLabel)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.TeleopGroupBox)
        Me.Controls.Add(Me.AutoModeGroupBox)
        Me.Controls.Add(Me.CloseButton)
        Me.Controls.Add(Me.RobotNumberLabel)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.RoundNumberLabel)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "RobotRoundReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Robot Round Report"
        Me.AutoModeGroupBox.ResumeLayout(False)
        Me.AutoModeGroupBox.PerformLayout()
        CType(Me.AutoScoredHighGoalPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AutoScoredLowGoalPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AutoCrossedObstaclePictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AutoReachedObstaclePictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TeleopGroupBox.ResumeLayout(False)
        Me.TeleopGroupBox.PerformLayout()
        CType(Me.TeleopRoughTerrainPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeleopClimbedWallsPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeleopScoredHighGoalsPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeleopScoredLowGoalsPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeleopPickedUpBallsPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeleopRockWallPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeleopSallyPortPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeleopDrawbridgePictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeleopRampartsPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeleopMoatPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeleopChevalDeFrisPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeleopPortcullisPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeleopLowBarPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents RoundNumberLabel As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents RobotNumberLabel As Label
    Friend WithEvents CloseButton As Button
    Friend WithEvents AutoModeGroupBox As GroupBox
    Friend WithEvents AutoScoredHighGoalPictureBox As PictureBox
    Friend WithEvents Label6 As Label
    Friend WithEvents AutoScoredLowGoalPictureBox As PictureBox
    Friend WithEvents AutoCrossedObstaclePictureBox As PictureBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents AutoReachedObstaclePictureBox As PictureBox
    Friend WithEvents Label3 As Label
    Friend WithEvents TeleopGroupBox As GroupBox
    Friend WithEvents TeleopRoughTerrainPictureBox As PictureBox
    Friend WithEvents Label19 As Label
    Friend WithEvents TeleopClimbedWallsPictureBox As PictureBox
    Friend WithEvents Label15 As Label
    Friend WithEvents TeleopScoredHighGoalsPictureBox As PictureBox
    Friend WithEvents TeleopScoredLowGoalsPictureBox As PictureBox
    Friend WithEvents Label16 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents TeleopPickedUpBallsPictureBox As PictureBox
    Friend WithEvents Label18 As Label
    Friend WithEvents TeleopRockWallPictureBox As PictureBox
    Friend WithEvents Label11 As Label
    Friend WithEvents TeleopSallyPortPictureBox As PictureBox
    Friend WithEvents TeleopDrawbridgePictureBox As PictureBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents TeleopRampartsPictureBox As PictureBox
    Friend WithEvents Label14 As Label
    Friend WithEvents TeleopMoatPictureBox As PictureBox
    Friend WithEvents Label7 As Label
    Friend WithEvents TeleopChevalDeFrisPictureBox As PictureBox
    Friend WithEvents TeleopPortcullisPictureBox As PictureBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents TeleopLowBarPictureBox As PictureBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents GoalsScoredLabel As Label
    Friend WithEvents CloseAndResetRoundButton As Button
End Class
