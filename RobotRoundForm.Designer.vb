﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class RobotRoundForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.RoundNumberTextBox = New System.Windows.Forms.TextBox()
        Me.RobotNumberTextBox = New System.Windows.Forms.TextBox()
        Me.FinishedButton = New System.Windows.Forms.Button()
        Me.AutoReachedObstacleLabel = New System.Windows.Forms.Label()
        Me.ModeTabControl = New System.Windows.Forms.TabControl()
        Me.AutoTabPage = New System.Windows.Forms.TabPage()
        Me.AutoNextButton = New System.Windows.Forms.Button()
        Me.AutoScoredHighGoalLabel = New System.Windows.Forms.Label()
        Me.AutoScoredLowGoalLabel = New System.Windows.Forms.Label()
        Me.AutoScoredHighGoalCheckBox = New System.Windows.Forms.CheckBox()
        Me.AutoReachedObstacleCheckBox = New System.Windows.Forms.CheckBox()
        Me.AutoScoredLowGoalCheckBox = New System.Windows.Forms.CheckBox()
        Me.AutoCrossedObstacleLabel = New System.Windows.Forms.Label()
        Me.AutoCrossedObstacleCheckBox = New System.Windows.Forms.CheckBox()
        Me.TeleopTabPage = New System.Windows.Forms.TabPage()
        Me.TeleopClimbedWallLabel = New System.Windows.Forms.Label()
        Me.TeleopScoredHighGoalsLabel = New System.Windows.Forms.Label()
        Me.TeleopScoredLowGoalsLabel = New System.Windows.Forms.Label()
        Me.TeleopPickedUpBallsLabel = New System.Windows.Forms.Label()
        Me.TeleopRoughTerrainLabel = New System.Windows.Forms.Label()
        Me.TeleopRockWallLabel = New System.Windows.Forms.Label()
        Me.TeleopSallyPortLabel = New System.Windows.Forms.Label()
        Me.TeleopDrawbridgeLabel = New System.Windows.Forms.Label()
        Me.TeleopRampartsLabel = New System.Windows.Forms.Label()
        Me.TeleopMoatLabel = New System.Windows.Forms.Label()
        Me.TeleopChevalDeFrisLabel = New System.Windows.Forms.Label()
        Me.TeleopPortcullisLabel = New System.Windows.Forms.Label()
        Me.TeleopLowBarLabel = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GoalsScoredTextBox = New System.Windows.Forms.TextBox()
        Me.TeleopClimbWallCheckBox = New System.Windows.Forms.CheckBox()
        Me.TeleopScoreHighGoalsCheckBox = New System.Windows.Forms.CheckBox()
        Me.TeleopScoreLowGoalsCheckBox = New System.Windows.Forms.CheckBox()
        Me.TeleopPickUpBallsCheckBox = New System.Windows.Forms.CheckBox()
        Me.TeleopRoughTerrainCheckBox = New System.Windows.Forms.CheckBox()
        Me.TeleopRockWallCheckBox = New System.Windows.Forms.CheckBox()
        Me.TeleopSallyPortCheckBox = New System.Windows.Forms.CheckBox()
        Me.TeleopDrawbridgeCheckBox = New System.Windows.Forms.CheckBox()
        Me.TeleopRampartsCheckBox = New System.Windows.Forms.CheckBox()
        Me.TeleopMoatCheckBox = New System.Windows.Forms.CheckBox()
        Me.TeleopChevalDeFrisCheckBox = New System.Windows.Forms.CheckBox()
        Me.TeleopPortcullisCheckBox = New System.Windows.Forms.CheckBox()
        Me.TeleopLowBarCheckBox = New System.Windows.Forms.CheckBox()
        Me.ObstaclesGroupBox = New System.Windows.Forms.GroupBox()
        Me.AbilitiesGroupBox = New System.Windows.Forms.GroupBox()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResetRoundToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CloseButton = New System.Windows.Forms.Button()
        Me.ResetRoundButton = New System.Windows.Forms.Button()
        Me.ModeTabControl.SuspendLayout()
        Me.AutoTabPage.SuspendLayout()
        Me.TeleopTabPage.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(10, 47)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(85, 18)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Round #:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(155, 47)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 18)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Robot #:"
        '
        'RoundNumberTextBox
        '
        Me.RoundNumberTextBox.Location = New System.Drawing.Point(96, 47)
        Me.RoundNumberTextBox.Name = "RoundNumberTextBox"
        Me.RoundNumberTextBox.Size = New System.Drawing.Size(40, 20)
        Me.RoundNumberTextBox.TabIndex = 3
        '
        'RobotNumberTextBox
        '
        Me.RobotNumberTextBox.Location = New System.Drawing.Point(236, 47)
        Me.RobotNumberTextBox.Name = "RobotNumberTextBox"
        Me.RobotNumberTextBox.Size = New System.Drawing.Size(60, 20)
        Me.RobotNumberTextBox.TabIndex = 4
        '
        'FinishedButton
        '
        Me.FinishedButton.Location = New System.Drawing.Point(480, 379)
        Me.FinishedButton.Name = "FinishedButton"
        Me.FinishedButton.Size = New System.Drawing.Size(89, 31)
        Me.FinishedButton.TabIndex = 5
        Me.FinishedButton.Text = "FINISHED"
        Me.FinishedButton.UseVisualStyleBackColor = True
        '
        'AutoReachedObstacleLabel
        '
        Me.AutoReachedObstacleLabel.AutoSize = True
        Me.AutoReachedObstacleLabel.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AutoReachedObstacleLabel.Location = New System.Drawing.Point(90, 18)
        Me.AutoReachedObstacleLabel.Name = "AutoReachedObstacleLabel"
        Me.AutoReachedObstacleLabel.Size = New System.Drawing.Size(152, 18)
        Me.AutoReachedObstacleLabel.TabIndex = 15
        Me.AutoReachedObstacleLabel.Text = "Reached obstacle"
        '
        'ModeTabControl
        '
        Me.ModeTabControl.Controls.Add(Me.AutoTabPage)
        Me.ModeTabControl.Controls.Add(Me.TeleopTabPage)
        Me.ModeTabControl.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ModeTabControl.Location = New System.Drawing.Point(12, 89)
        Me.ModeTabControl.Name = "ModeTabControl"
        Me.ModeTabControl.SelectedIndex = 0
        Me.ModeTabControl.Size = New System.Drawing.Size(583, 445)
        Me.ModeTabControl.TabIndex = 17
        '
        'AutoTabPage
        '
        Me.AutoTabPage.BackColor = System.Drawing.Color.Gainsboro
        Me.AutoTabPage.Controls.Add(Me.AutoNextButton)
        Me.AutoTabPage.Controls.Add(Me.AutoScoredHighGoalLabel)
        Me.AutoTabPage.Controls.Add(Me.AutoScoredLowGoalLabel)
        Me.AutoTabPage.Controls.Add(Me.AutoScoredHighGoalCheckBox)
        Me.AutoTabPage.Controls.Add(Me.AutoReachedObstacleCheckBox)
        Me.AutoTabPage.Controls.Add(Me.AutoScoredLowGoalCheckBox)
        Me.AutoTabPage.Controls.Add(Me.AutoCrossedObstacleLabel)
        Me.AutoTabPage.Controls.Add(Me.AutoReachedObstacleLabel)
        Me.AutoTabPage.Controls.Add(Me.AutoCrossedObstacleCheckBox)
        Me.AutoTabPage.Location = New System.Drawing.Point(4, 25)
        Me.AutoTabPage.Name = "AutoTabPage"
        Me.AutoTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me.AutoTabPage.Size = New System.Drawing.Size(575, 416)
        Me.AutoTabPage.TabIndex = 0
        Me.AutoTabPage.Text = "Autonomous Mode"
        '
        'AutoNextButton
        '
        Me.AutoNextButton.Location = New System.Drawing.Point(479, 377)
        Me.AutoNextButton.Name = "AutoNextButton"
        Me.AutoNextButton.Size = New System.Drawing.Size(75, 23)
        Me.AutoNextButton.TabIndex = 26
        Me.AutoNextButton.Text = "Next >>"
        Me.AutoNextButton.UseVisualStyleBackColor = True
        '
        'AutoScoredHighGoalLabel
        '
        Me.AutoScoredHighGoalLabel.AutoSize = True
        Me.AutoScoredHighGoalLabel.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AutoScoredHighGoalLabel.Location = New System.Drawing.Point(90, 168)
        Me.AutoScoredHighGoalLabel.Name = "AutoScoredHighGoalLabel"
        Me.AutoScoredHighGoalLabel.Size = New System.Drawing.Size(144, 18)
        Me.AutoScoredHighGoalLabel.TabIndex = 25
        Me.AutoScoredHighGoalLabel.Text = "Scored high goal"
        '
        'AutoScoredLowGoalLabel
        '
        Me.AutoScoredLowGoalLabel.AutoSize = True
        Me.AutoScoredLowGoalLabel.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AutoScoredLowGoalLabel.Location = New System.Drawing.Point(90, 118)
        Me.AutoScoredLowGoalLabel.Name = "AutoScoredLowGoalLabel"
        Me.AutoScoredLowGoalLabel.Size = New System.Drawing.Size(137, 18)
        Me.AutoScoredLowGoalLabel.TabIndex = 24
        Me.AutoScoredLowGoalLabel.Text = "Scored low goal"
        '
        'AutoScoredHighGoalCheckBox
        '
        Me.AutoScoredHighGoalCheckBox.Location = New System.Drawing.Point(10, 160)
        Me.AutoScoredHighGoalCheckBox.Name = "AutoScoredHighGoalCheckBox"
        Me.AutoScoredHighGoalCheckBox.Size = New System.Drawing.Size(80, 34)
        Me.AutoScoredHighGoalCheckBox.TabIndex = 23
        Me.AutoScoredHighGoalCheckBox.UseVisualStyleBackColor = True
        '
        'AutoReachedObstacleCheckBox
        '
        Me.AutoReachedObstacleCheckBox.Location = New System.Drawing.Point(10, 10)
        Me.AutoReachedObstacleCheckBox.Name = "AutoReachedObstacleCheckBox"
        Me.AutoReachedObstacleCheckBox.Size = New System.Drawing.Size(80, 34)
        Me.AutoReachedObstacleCheckBox.TabIndex = 18
        Me.AutoReachedObstacleCheckBox.UseVisualStyleBackColor = True
        '
        'AutoScoredLowGoalCheckBox
        '
        Me.AutoScoredLowGoalCheckBox.Location = New System.Drawing.Point(10, 110)
        Me.AutoScoredLowGoalCheckBox.Name = "AutoScoredLowGoalCheckBox"
        Me.AutoScoredLowGoalCheckBox.Size = New System.Drawing.Size(80, 34)
        Me.AutoScoredLowGoalCheckBox.TabIndex = 22
        Me.AutoScoredLowGoalCheckBox.UseVisualStyleBackColor = True
        '
        'AutoCrossedObstacleLabel
        '
        Me.AutoCrossedObstacleLabel.AutoSize = True
        Me.AutoCrossedObstacleLabel.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AutoCrossedObstacleLabel.Location = New System.Drawing.Point(90, 68)
        Me.AutoCrossedObstacleLabel.Name = "AutoCrossedObstacleLabel"
        Me.AutoCrossedObstacleLabel.Size = New System.Drawing.Size(148, 18)
        Me.AutoCrossedObstacleLabel.TabIndex = 19
        Me.AutoCrossedObstacleLabel.Text = "Crossed obstacle"
        '
        'AutoCrossedObstacleCheckBox
        '
        Me.AutoCrossedObstacleCheckBox.Location = New System.Drawing.Point(10, 60)
        Me.AutoCrossedObstacleCheckBox.Name = "AutoCrossedObstacleCheckBox"
        Me.AutoCrossedObstacleCheckBox.Size = New System.Drawing.Size(80, 34)
        Me.AutoCrossedObstacleCheckBox.TabIndex = 21
        Me.AutoCrossedObstacleCheckBox.UseVisualStyleBackColor = True
        '
        'TeleopTabPage
        '
        Me.TeleopTabPage.BackColor = System.Drawing.Color.Gainsboro
        Me.TeleopTabPage.Controls.Add(Me.TeleopClimbedWallLabel)
        Me.TeleopTabPage.Controls.Add(Me.FinishedButton)
        Me.TeleopTabPage.Controls.Add(Me.TeleopScoredHighGoalsLabel)
        Me.TeleopTabPage.Controls.Add(Me.TeleopScoredLowGoalsLabel)
        Me.TeleopTabPage.Controls.Add(Me.TeleopPickedUpBallsLabel)
        Me.TeleopTabPage.Controls.Add(Me.TeleopRoughTerrainLabel)
        Me.TeleopTabPage.Controls.Add(Me.TeleopRockWallLabel)
        Me.TeleopTabPage.Controls.Add(Me.TeleopSallyPortLabel)
        Me.TeleopTabPage.Controls.Add(Me.TeleopDrawbridgeLabel)
        Me.TeleopTabPage.Controls.Add(Me.TeleopRampartsLabel)
        Me.TeleopTabPage.Controls.Add(Me.TeleopMoatLabel)
        Me.TeleopTabPage.Controls.Add(Me.TeleopChevalDeFrisLabel)
        Me.TeleopTabPage.Controls.Add(Me.TeleopPortcullisLabel)
        Me.TeleopTabPage.Controls.Add(Me.TeleopLowBarLabel)
        Me.TeleopTabPage.Controls.Add(Me.Label4)
        Me.TeleopTabPage.Controls.Add(Me.GoalsScoredTextBox)
        Me.TeleopTabPage.Controls.Add(Me.TeleopClimbWallCheckBox)
        Me.TeleopTabPage.Controls.Add(Me.TeleopScoreHighGoalsCheckBox)
        Me.TeleopTabPage.Controls.Add(Me.TeleopScoreLowGoalsCheckBox)
        Me.TeleopTabPage.Controls.Add(Me.TeleopPickUpBallsCheckBox)
        Me.TeleopTabPage.Controls.Add(Me.TeleopRoughTerrainCheckBox)
        Me.TeleopTabPage.Controls.Add(Me.TeleopRockWallCheckBox)
        Me.TeleopTabPage.Controls.Add(Me.TeleopSallyPortCheckBox)
        Me.TeleopTabPage.Controls.Add(Me.TeleopDrawbridgeCheckBox)
        Me.TeleopTabPage.Controls.Add(Me.TeleopRampartsCheckBox)
        Me.TeleopTabPage.Controls.Add(Me.TeleopMoatCheckBox)
        Me.TeleopTabPage.Controls.Add(Me.TeleopChevalDeFrisCheckBox)
        Me.TeleopTabPage.Controls.Add(Me.TeleopPortcullisCheckBox)
        Me.TeleopTabPage.Controls.Add(Me.TeleopLowBarCheckBox)
        Me.TeleopTabPage.Controls.Add(Me.ObstaclesGroupBox)
        Me.TeleopTabPage.Controls.Add(Me.AbilitiesGroupBox)
        Me.TeleopTabPage.Location = New System.Drawing.Point(4, 25)
        Me.TeleopTabPage.Name = "TeleopTabPage"
        Me.TeleopTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me.TeleopTabPage.Size = New System.Drawing.Size(575, 416)
        Me.TeleopTabPage.TabIndex = 1
        Me.TeleopTabPage.Text = "Teleop Mode"
        '
        'TeleopClimbedWallLabel
        '
        Me.TeleopClimbedWallLabel.AutoSize = True
        Me.TeleopClimbedWallLabel.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TeleopClimbedWallLabel.Location = New System.Drawing.Point(404, 172)
        Me.TeleopClimbedWallLabel.Name = "TeleopClimbedWallLabel"
        Me.TeleopClimbedWallLabel.Size = New System.Drawing.Size(113, 18)
        Me.TeleopClimbedWallLabel.TabIndex = 50
        Me.TeleopClimbedWallLabel.Text = "Climbed wall"
        '
        'TeleopScoredHighGoalsLabel
        '
        Me.TeleopScoredHighGoalsLabel.AutoSize = True
        Me.TeleopScoredHighGoalsLabel.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TeleopScoredHighGoalsLabel.Location = New System.Drawing.Point(404, 132)
        Me.TeleopScoredHighGoalsLabel.Name = "TeleopScoredHighGoalsLabel"
        Me.TeleopScoredHighGoalsLabel.Size = New System.Drawing.Size(153, 18)
        Me.TeleopScoredHighGoalsLabel.TabIndex = 49
        Me.TeleopScoredHighGoalsLabel.Text = "Scored high goals"
        '
        'TeleopScoredLowGoalsLabel
        '
        Me.TeleopScoredLowGoalsLabel.AutoSize = True
        Me.TeleopScoredLowGoalsLabel.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TeleopScoredLowGoalsLabel.Location = New System.Drawing.Point(404, 92)
        Me.TeleopScoredLowGoalsLabel.Name = "TeleopScoredLowGoalsLabel"
        Me.TeleopScoredLowGoalsLabel.Size = New System.Drawing.Size(146, 18)
        Me.TeleopScoredLowGoalsLabel.TabIndex = 48
        Me.TeleopScoredLowGoalsLabel.Text = "Scored low goals"
        '
        'TeleopPickedUpBallsLabel
        '
        Me.TeleopPickedUpBallsLabel.AutoSize = True
        Me.TeleopPickedUpBallsLabel.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TeleopPickedUpBallsLabel.Location = New System.Drawing.Point(404, 52)
        Me.TeleopPickedUpBallsLabel.Name = "TeleopPickedUpBallsLabel"
        Me.TeleopPickedUpBallsLabel.Size = New System.Drawing.Size(131, 18)
        Me.TeleopPickedUpBallsLabel.TabIndex = 47
        Me.TeleopPickedUpBallsLabel.Text = "Picked up balls"
        '
        'TeleopRoughTerrainLabel
        '
        Me.TeleopRoughTerrainLabel.AutoSize = True
        Me.TeleopRoughTerrainLabel.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TeleopRoughTerrainLabel.Location = New System.Drawing.Point(100, 368)
        Me.TeleopRoughTerrainLabel.Name = "TeleopRoughTerrainLabel"
        Me.TeleopRoughTerrainLabel.Size = New System.Drawing.Size(122, 18)
        Me.TeleopRoughTerrainLabel.TabIndex = 46
        Me.TeleopRoughTerrainLabel.Text = "Rough Terrain"
        '
        'TeleopRockWallLabel
        '
        Me.TeleopRockWallLabel.AutoSize = True
        Me.TeleopRockWallLabel.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TeleopRockWallLabel.Location = New System.Drawing.Point(100, 328)
        Me.TeleopRockWallLabel.Name = "TeleopRockWallLabel"
        Me.TeleopRockWallLabel.Size = New System.Drawing.Size(89, 18)
        Me.TeleopRockWallLabel.TabIndex = 45
        Me.TeleopRockWallLabel.Text = "Rock Wall"
        '
        'TeleopSallyPortLabel
        '
        Me.TeleopSallyPortLabel.AutoSize = True
        Me.TeleopSallyPortLabel.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TeleopSallyPortLabel.Location = New System.Drawing.Point(100, 288)
        Me.TeleopSallyPortLabel.Name = "TeleopSallyPortLabel"
        Me.TeleopSallyPortLabel.Size = New System.Drawing.Size(86, 18)
        Me.TeleopSallyPortLabel.TabIndex = 44
        Me.TeleopSallyPortLabel.Text = "Sally Port"
        '
        'TeleopDrawbridgeLabel
        '
        Me.TeleopDrawbridgeLabel.AutoSize = True
        Me.TeleopDrawbridgeLabel.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TeleopDrawbridgeLabel.Location = New System.Drawing.Point(100, 248)
        Me.TeleopDrawbridgeLabel.Name = "TeleopDrawbridgeLabel"
        Me.TeleopDrawbridgeLabel.Size = New System.Drawing.Size(100, 18)
        Me.TeleopDrawbridgeLabel.TabIndex = 43
        Me.TeleopDrawbridgeLabel.Text = "Drawbridge"
        '
        'TeleopRampartsLabel
        '
        Me.TeleopRampartsLabel.AutoSize = True
        Me.TeleopRampartsLabel.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TeleopRampartsLabel.Location = New System.Drawing.Point(100, 208)
        Me.TeleopRampartsLabel.Name = "TeleopRampartsLabel"
        Me.TeleopRampartsLabel.Size = New System.Drawing.Size(86, 18)
        Me.TeleopRampartsLabel.TabIndex = 42
        Me.TeleopRampartsLabel.Text = "Ramparts"
        '
        'TeleopMoatLabel
        '
        Me.TeleopMoatLabel.AutoSize = True
        Me.TeleopMoatLabel.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TeleopMoatLabel.Location = New System.Drawing.Point(100, 168)
        Me.TeleopMoatLabel.Name = "TeleopMoatLabel"
        Me.TeleopMoatLabel.Size = New System.Drawing.Size(48, 18)
        Me.TeleopMoatLabel.TabIndex = 41
        Me.TeleopMoatLabel.Text = "Moat"
        '
        'TeleopChevalDeFrisLabel
        '
        Me.TeleopChevalDeFrisLabel.AutoSize = True
        Me.TeleopChevalDeFrisLabel.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TeleopChevalDeFrisLabel.Location = New System.Drawing.Point(100, 128)
        Me.TeleopChevalDeFrisLabel.Name = "TeleopChevalDeFrisLabel"
        Me.TeleopChevalDeFrisLabel.Size = New System.Drawing.Size(124, 18)
        Me.TeleopChevalDeFrisLabel.TabIndex = 40
        Me.TeleopChevalDeFrisLabel.Text = "Cheval de Fris"
        '
        'TeleopPortcullisLabel
        '
        Me.TeleopPortcullisLabel.AutoSize = True
        Me.TeleopPortcullisLabel.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TeleopPortcullisLabel.Location = New System.Drawing.Point(100, 88)
        Me.TeleopPortcullisLabel.Name = "TeleopPortcullisLabel"
        Me.TeleopPortcullisLabel.Size = New System.Drawing.Size(83, 18)
        Me.TeleopPortcullisLabel.TabIndex = 39
        Me.TeleopPortcullisLabel.Text = "Portcullis"
        '
        'TeleopLowBarLabel
        '
        Me.TeleopLowBarLabel.AutoSize = True
        Me.TeleopLowBarLabel.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TeleopLowBarLabel.Location = New System.Drawing.Point(100, 48)
        Me.TeleopLowBarLabel.Name = "TeleopLowBarLabel"
        Me.TeleopLowBarLabel.Size = New System.Drawing.Size(72, 18)
        Me.TeleopLowBarLabel.TabIndex = 38
        Me.TeleopLowBarLabel.Text = "Low bar"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(343, 257)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(98, 16)
        Me.Label4.TabIndex = 37
        Me.Label4.Text = "Goals scored:"
        '
        'GoalsScoredTextBox
        '
        Me.GoalsScoredTextBox.Location = New System.Drawing.Point(460, 254)
        Me.GoalsScoredTextBox.Name = "GoalsScoredTextBox"
        Me.GoalsScoredTextBox.Size = New System.Drawing.Size(100, 23)
        Me.GoalsScoredTextBox.TabIndex = 36
        '
        'TeleopClimbWallCheckBox
        '
        Me.TeleopClimbWallCheckBox.Location = New System.Drawing.Point(318, 162)
        Me.TeleopClimbWallCheckBox.Name = "TeleopClimbWallCheckBox"
        Me.TeleopClimbWallCheckBox.Size = New System.Drawing.Size(80, 34)
        Me.TeleopClimbWallCheckBox.TabIndex = 35
        Me.TeleopClimbWallCheckBox.UseVisualStyleBackColor = True
        '
        'TeleopScoreHighGoalsCheckBox
        '
        Me.TeleopScoreHighGoalsCheckBox.Location = New System.Drawing.Point(318, 122)
        Me.TeleopScoreHighGoalsCheckBox.Name = "TeleopScoreHighGoalsCheckBox"
        Me.TeleopScoreHighGoalsCheckBox.Size = New System.Drawing.Size(80, 34)
        Me.TeleopScoreHighGoalsCheckBox.TabIndex = 34
        Me.TeleopScoreHighGoalsCheckBox.UseVisualStyleBackColor = True
        '
        'TeleopScoreLowGoalsCheckBox
        '
        Me.TeleopScoreLowGoalsCheckBox.Location = New System.Drawing.Point(318, 82)
        Me.TeleopScoreLowGoalsCheckBox.Name = "TeleopScoreLowGoalsCheckBox"
        Me.TeleopScoreLowGoalsCheckBox.Size = New System.Drawing.Size(80, 34)
        Me.TeleopScoreLowGoalsCheckBox.TabIndex = 33
        Me.TeleopScoreLowGoalsCheckBox.UseVisualStyleBackColor = True
        '
        'TeleopPickUpBallsCheckBox
        '
        Me.TeleopPickUpBallsCheckBox.Location = New System.Drawing.Point(318, 42)
        Me.TeleopPickUpBallsCheckBox.Name = "TeleopPickUpBallsCheckBox"
        Me.TeleopPickUpBallsCheckBox.Size = New System.Drawing.Size(80, 34)
        Me.TeleopPickUpBallsCheckBox.TabIndex = 32
        Me.TeleopPickUpBallsCheckBox.UseVisualStyleBackColor = True
        '
        'TeleopRoughTerrainCheckBox
        '
        Me.TeleopRoughTerrainCheckBox.Location = New System.Drawing.Point(20, 360)
        Me.TeleopRoughTerrainCheckBox.Name = "TeleopRoughTerrainCheckBox"
        Me.TeleopRoughTerrainCheckBox.Size = New System.Drawing.Size(80, 34)
        Me.TeleopRoughTerrainCheckBox.TabIndex = 31
        Me.TeleopRoughTerrainCheckBox.UseVisualStyleBackColor = True
        '
        'TeleopRockWallCheckBox
        '
        Me.TeleopRockWallCheckBox.Location = New System.Drawing.Point(20, 320)
        Me.TeleopRockWallCheckBox.Name = "TeleopRockWallCheckBox"
        Me.TeleopRockWallCheckBox.Size = New System.Drawing.Size(80, 34)
        Me.TeleopRockWallCheckBox.TabIndex = 30
        Me.TeleopRockWallCheckBox.UseVisualStyleBackColor = True
        '
        'TeleopSallyPortCheckBox
        '
        Me.TeleopSallyPortCheckBox.Location = New System.Drawing.Point(20, 280)
        Me.TeleopSallyPortCheckBox.Name = "TeleopSallyPortCheckBox"
        Me.TeleopSallyPortCheckBox.Size = New System.Drawing.Size(80, 34)
        Me.TeleopSallyPortCheckBox.TabIndex = 29
        Me.TeleopSallyPortCheckBox.UseVisualStyleBackColor = True
        '
        'TeleopDrawbridgeCheckBox
        '
        Me.TeleopDrawbridgeCheckBox.Location = New System.Drawing.Point(20, 240)
        Me.TeleopDrawbridgeCheckBox.Name = "TeleopDrawbridgeCheckBox"
        Me.TeleopDrawbridgeCheckBox.Size = New System.Drawing.Size(80, 34)
        Me.TeleopDrawbridgeCheckBox.TabIndex = 29
        Me.TeleopDrawbridgeCheckBox.UseVisualStyleBackColor = True
        '
        'TeleopRampartsCheckBox
        '
        Me.TeleopRampartsCheckBox.Location = New System.Drawing.Point(20, 200)
        Me.TeleopRampartsCheckBox.Name = "TeleopRampartsCheckBox"
        Me.TeleopRampartsCheckBox.Size = New System.Drawing.Size(80, 34)
        Me.TeleopRampartsCheckBox.TabIndex = 28
        Me.TeleopRampartsCheckBox.UseVisualStyleBackColor = True
        '
        'TeleopMoatCheckBox
        '
        Me.TeleopMoatCheckBox.Location = New System.Drawing.Point(20, 160)
        Me.TeleopMoatCheckBox.Name = "TeleopMoatCheckBox"
        Me.TeleopMoatCheckBox.Size = New System.Drawing.Size(80, 34)
        Me.TeleopMoatCheckBox.TabIndex = 27
        Me.TeleopMoatCheckBox.UseVisualStyleBackColor = True
        '
        'TeleopChevalDeFrisCheckBox
        '
        Me.TeleopChevalDeFrisCheckBox.Location = New System.Drawing.Point(20, 120)
        Me.TeleopChevalDeFrisCheckBox.Name = "TeleopChevalDeFrisCheckBox"
        Me.TeleopChevalDeFrisCheckBox.Size = New System.Drawing.Size(80, 34)
        Me.TeleopChevalDeFrisCheckBox.TabIndex = 26
        Me.TeleopChevalDeFrisCheckBox.UseVisualStyleBackColor = True
        '
        'TeleopPortcullisCheckBox
        '
        Me.TeleopPortcullisCheckBox.Location = New System.Drawing.Point(20, 80)
        Me.TeleopPortcullisCheckBox.Name = "TeleopPortcullisCheckBox"
        Me.TeleopPortcullisCheckBox.Size = New System.Drawing.Size(80, 34)
        Me.TeleopPortcullisCheckBox.TabIndex = 25
        Me.TeleopPortcullisCheckBox.UseVisualStyleBackColor = True
        '
        'TeleopLowBarCheckBox
        '
        Me.TeleopLowBarCheckBox.Location = New System.Drawing.Point(20, 40)
        Me.TeleopLowBarCheckBox.Name = "TeleopLowBarCheckBox"
        Me.TeleopLowBarCheckBox.Size = New System.Drawing.Size(80, 34)
        Me.TeleopLowBarCheckBox.TabIndex = 24
        Me.TeleopLowBarCheckBox.UseVisualStyleBackColor = True
        '
        'ObstaclesGroupBox
        '
        Me.ObstaclesGroupBox.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ObstaclesGroupBox.Location = New System.Drawing.Point(6, 12)
        Me.ObstaclesGroupBox.Name = "ObstaclesGroupBox"
        Me.ObstaclesGroupBox.Size = New System.Drawing.Size(279, 401)
        Me.ObstaclesGroupBox.TabIndex = 51
        Me.ObstaclesGroupBox.TabStop = False
        Me.ObstaclesGroupBox.Text = "Past Obstacles (select ones defeated)"
        '
        'AbilitiesGroupBox
        '
        Me.AbilitiesGroupBox.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AbilitiesGroupBox.Location = New System.Drawing.Point(299, 16)
        Me.AbilitiesGroupBox.Name = "AbilitiesGroupBox"
        Me.AbilitiesGroupBox.Size = New System.Drawing.Size(270, 202)
        Me.AbilitiesGroupBox.TabIndex = 52
        Me.AbilitiesGroupBox.TabStop = False
        Me.AbilitiesGroupBox.Text = "Robot Abilities (select ones viewed)"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem1})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(607, 24)
        Me.MenuStrip1.TabIndex = 18
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem1
        '
        Me.FileToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ResetRoundToolStripMenuItem, Me.ExitToolStripMenuItem1})
        Me.FileToolStripMenuItem1.Name = "FileToolStripMenuItem1"
        Me.FileToolStripMenuItem1.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem1.Text = "File"
        '
        'ResetRoundToolStripMenuItem
        '
        Me.ResetRoundToolStripMenuItem.Name = "ResetRoundToolStripMenuItem"
        Me.ResetRoundToolStripMenuItem.Size = New System.Drawing.Size(140, 22)
        Me.ResetRoundToolStripMenuItem.Text = "Reset Round"
        '
        'ExitToolStripMenuItem1
        '
        Me.ExitToolStripMenuItem1.Name = "ExitToolStripMenuItem1"
        Me.ExitToolStripMenuItem1.Size = New System.Drawing.Size(140, 22)
        Me.ExitToolStripMenuItem1.Text = "Exit"
        '
        'CloseButton
        '
        Me.CloseButton.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CloseButton.Location = New System.Drawing.Point(516, 545)
        Me.CloseButton.Name = "CloseButton"
        Me.CloseButton.Size = New System.Drawing.Size(75, 23)
        Me.CloseButton.TabIndex = 19
        Me.CloseButton.Text = "Close"
        Me.CloseButton.UseVisualStyleBackColor = True
        '
        'ResetRoundButton
        '
        Me.ResetRoundButton.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ResetRoundButton.Location = New System.Drawing.Point(216, 545)
        Me.ResetRoundButton.Name = "ResetRoundButton"
        Me.ResetRoundButton.Size = New System.Drawing.Size(132, 23)
        Me.ResetRoundButton.TabIndex = 20
        Me.ResetRoundButton.Text = "RESET ROUND"
        Me.ResetRoundButton.UseVisualStyleBackColor = True
        '
        'RobotRoundForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(607, 580)
        Me.Controls.Add(Me.ResetRoundButton)
        Me.Controls.Add(Me.CloseButton)
        Me.Controls.Add(Me.ModeTabControl)
        Me.Controls.Add(Me.RobotNumberTextBox)
        Me.Controls.Add(Me.RoundNumberTextBox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "RobotRoundForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Robot Round"
        Me.ModeTabControl.ResumeLayout(False)
        Me.AutoTabPage.ResumeLayout(False)
        Me.AutoTabPage.PerformLayout()
        Me.TeleopTabPage.ResumeLayout(False)
        Me.TeleopTabPage.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents RoundNumberTextBox As TextBox
    Friend WithEvents RobotNumberTextBox As TextBox
    Friend WithEvents FinishedButton As Button
    Friend WithEvents AutoReachedObstacleLabel As Label
    Friend WithEvents ModeTabControl As TabControl
    Friend WithEvents AutoTabPage As TabPage
    Friend WithEvents TeleopTabPage As TabPage
    Friend WithEvents AutoCrossedObstacleCheckBox As CheckBox
    Friend WithEvents AutoCrossedObstacleLabel As Label
    Friend WithEvents AutoScoredHighGoalLabel As Label
    Friend WithEvents AutoScoredLowGoalLabel As Label
    Friend WithEvents AutoScoredHighGoalCheckBox As CheckBox
    Friend WithEvents AutoReachedObstacleCheckBox As CheckBox
    Friend WithEvents AutoScoredLowGoalCheckBox As CheckBox
    Friend WithEvents TeleopClimbedWallLabel As Label
    Friend WithEvents TeleopScoredHighGoalsLabel As Label
    Friend WithEvents TeleopScoredLowGoalsLabel As Label
    Friend WithEvents TeleopPickedUpBallsLabel As Label
    Friend WithEvents TeleopRoughTerrainLabel As Label
    Friend WithEvents TeleopRockWallLabel As Label
    Friend WithEvents TeleopSallyPortLabel As Label
    Friend WithEvents TeleopDrawbridgeLabel As Label
    Friend WithEvents TeleopRampartsLabel As Label
    Friend WithEvents TeleopMoatLabel As Label
    Friend WithEvents TeleopChevalDeFrisLabel As Label
    Friend WithEvents TeleopPortcullisLabel As Label
    Friend WithEvents TeleopLowBarLabel As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents GoalsScoredTextBox As TextBox
    Friend WithEvents TeleopClimbWallCheckBox As CheckBox
    Friend WithEvents TeleopScoreHighGoalsCheckBox As CheckBox
    Friend WithEvents TeleopScoreLowGoalsCheckBox As CheckBox
    Friend WithEvents TeleopPickUpBallsCheckBox As CheckBox
    Friend WithEvents TeleopRoughTerrainCheckBox As CheckBox
    Friend WithEvents TeleopRockWallCheckBox As CheckBox
    Friend WithEvents TeleopSallyPortCheckBox As CheckBox
    Friend WithEvents TeleopDrawbridgeCheckBox As CheckBox
    Friend WithEvents TeleopRampartsCheckBox As CheckBox
    Friend WithEvents TeleopMoatCheckBox As CheckBox
    Friend WithEvents TeleopChevalDeFrisCheckBox As CheckBox
    Friend WithEvents TeleopPortcullisCheckBox As CheckBox
    Friend WithEvents TeleopLowBarCheckBox As CheckBox
    Friend WithEvents AutoNextButton As Button
    Friend WithEvents ObstaclesGroupBox As GroupBox
    Friend WithEvents AbilitiesGroupBox As GroupBox
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents FileToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents CloseButton As Button
    Friend WithEvents ResetRoundToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ResetRoundButton As Button
End Class
