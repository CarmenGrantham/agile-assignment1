﻿Public Class RobotRoundForm

    Dim RobotRound As RobotRound = New RobotRound()

    Public Sub New()
        ' This call is required by the designer.
        InitializeComponent()

    End Sub

    Private Sub RoundNumberTextBox_Leave(sender As Object, e As EventArgs) Handles RoundNumberTextBox.Leave
        ' After exiting the Round Number text box validate that it 
        ' - has a value
        ' - value is an integer
        ' - value is between Competition.FIRST_ROUND_NUMBER and
        '   Competition.LAST_ROUND_NUMBER

        Dim roundNumber As Int32
        Dim validRoundNumber As Boolean = False

        If (Not String.IsNullOrWhiteSpace(RoundNumberTextBox.Text)) Then
            ' Round number has a value
            Try
                ' This will throw an exception if value is not an integer
                roundNumber = Convert.ToInt32(RoundNumberTextBox.Text)

                ' When round number outside of valid values raise show error 
                If (roundNumber < Competition.FIRST_ROUND_NUMBER Or
                        roundNumber > Competition.LAST_ROUND_NUMBER) Then

                    ShowErrorMessage("Invalid Round Number entered. It must be between " &
                        Competition.FIRST_ROUND_NUMBER & " and " &
                        Competition.LAST_ROUND_NUMBER)
                Else
                    validRoundNumber = True
                End If
            Catch ex As Exception
                ShowErrorMessage("Round Number must be a number.")
            End Try

            If (Not validRoundNumber) Then
                ' Remove value from Round Number text box as it is invalid
                Me.RoundNumberTextBox.Text = ""

                ' Reset focus to Round Number text box so correct value can be entered
                ' before proceeding
                Me.RoundNumberTextBox.Focus()
            End If
        End If
    End Sub

    Private Sub FinishedButton_Click(sender As Object, e As EventArgs) Handles FinishedButton.Click
        ' Validate all fields, if anything fails show error message and 
        ' do not proceed any further
        ' - Round ID, Robot ID and Goals scored are required
        ' - Round ID and Goals Scored must have certain values but these are 
        '   checked after input and don't need to be re-validated here
        ' When validation successful copy all values to RobotRound and
        ' display Round Report form


        If (String.IsNullOrWhiteSpace(Me.RoundNumberTextBox.Text)) Then
            ShowErrorMessage("Round Number is required.")
            Me.RoundNumberTextBox.Focus()
            Return
        End If

        If (String.IsNullOrWhiteSpace(Me.RobotNumberTextBox.Text)) Then
            ShowErrorMessage("Robot Number is required.")
            Me.RobotNumberTextBox.Focus()
            Return
        End If

        If (String.IsNullOrWhiteSpace(Me.GoalsScoredTextBox.Text)) Then
            ShowErrorMessage("Goals Scored is required. Use 0 if there were no goals.")
            Me.GoalsScoredTextBox.Focus()
            Return
        End If



        ' When validation passed move contents of form to RobotRound class

        ' Round Number already validated as not null integer that is between minimum and maximum allowed values
        RobotRound.RoundId = Convert.ToInt32(Me.RoundNumberTextBox.Text)

        ' Get Robot number which is just a string
        RobotRound.RobotId = Me.RobotNumberTextBox.Text

        ' Store all the Autonomous mode options
        RobotRound.AutoReachedObstacle = Me.AutoReachedObstacleCheckBox.Checked
        RobotRound.AutoCrossedObstacle = Me.AutoCrossedObstacleCheckBox.Checked
        RobotRound.AutoScoredLowGoal = Me.AutoScoredLowGoalCheckBox.Checked
        RobotRound.AutoScoredHighGoal = Me.AutoScoredHighGoalCheckBox.Checked

        ' Store all the Teleop obstacles
        RobotRound.TeleopLowBar = Me.TeleopLowBarCheckBox.Checked
        RobotRound.TeleopPortcullis = Me.TeleopPortcullisCheckBox.Checked
        RobotRound.TeleopChevalDeFris = Me.TeleopChevalDeFrisCheckBox.Checked
        RobotRound.TeleopMoat = Me.TeleopMoatCheckBox.Checked
        RobotRound.TeleopRamparts = Me.TeleopRampartsCheckBox.Checked
        RobotRound.TeleopDrawbridge = Me.TeleopDrawbridgeCheckBox.Checked
        RobotRound.TeleopSallyPort = Me.TeleopSallyPortCheckBox.Checked
        RobotRound.TeleopRockWall = Me.TeleopRockWallCheckBox.Checked
        RobotRound.TeleopRoughTerrain = Me.TeleopRoughTerrainCheckBox.Checked

        ' Store all the Teleop Robot abilities
        RobotRound.TeleopPickUpBalls = Me.TeleopPickUpBallsCheckBox.Checked
        RobotRound.TeleopScoreLowGoals = Me.TeleopScoreLowGoalsCheckBox.Checked
        RobotRound.TeleopScoreHighGoals = Me.TeleopScoreHighGoalsCheckBox.Checked
        RobotRound.TeleopClimbWall = Me.TeleopClimbWallCheckBox.Checked

        ' Goals scored already validated as not null integer that is 0 or more
        RobotRound.GoalsScored = Convert.ToInt32(Me.GoalsScoredTextBox.Text)

        ' Display summary form using RobotRound as a dialog, so it must be
        ' closed before this form can be accessed again
        Dim report = New RobotRoundReport(Me, Me.RobotRound)
        report.ShowDialog()
    End Sub

    Private Sub GenericCheckbox_Paint(sender As Object, e As PaintEventArgs) Handles AutoCrossedObstacleCheckBox.Paint, AutoScoredLowGoalCheckBox.Paint, AutoScoredHighGoalCheckBox.Paint, AutoReachedObstacleCheckBox.Paint, TeleopLowBarCheckBox.Paint, TeleopRoughTerrainCheckBox.Paint, TeleopRockWallCheckBox.Paint, TeleopSallyPortCheckBox.Paint, TeleopDrawbridgeCheckBox.Paint, TeleopRampartsCheckBox.Paint, TeleopMoatCheckBox.Paint, TeleopChevalDeFrisCheckBox.Paint, TeleopPortcullisCheckBox.Paint, TeleopPickUpBallsCheckBox.Paint, TeleopScoreLowGoalsCheckBox.Paint, TeleopScoreHighGoalsCheckBox.Paint, TeleopClimbWallCheckBox.Paint
        ' Handles the image that is displayed on checkboxes and is dependant
        ' on if checkbox is checked or not.

        ' Image reference
        '        We Love Solo, (2014), Inset On Off Toggle Switch Set, 
        '        viewed 24 March 2016 <https: 
        '        //www.welovesolo.com/inset-on-off-toggle-switch-set/>

        If sender.Checked Then
            e.Graphics.DrawImageUnscaled(My.Resources.checkbox_checked, 0, 0)
        Else
            e.Graphics.DrawImage(My.Resources.checkbox_unchecked, 0, 0)
        End If
    End Sub

    Private Sub AutoNextButton_Click(sender As Object, e As EventArgs) Handles AutoNextButton.Click
        ' Change Tab Control to the Teleop tab
        Me.ModeTabControl.SelectedTab = Me.TeleopTabPage
    End Sub

    Private Sub ExitToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem1.Click
        CloseForm()
    End Sub

    Private Sub GoalsScoredTextBox_Leave(sender As Object, e As EventArgs) Handles GoalsScoredTextBox.Leave
        ' After exiting the Goals Scored text box validate that it 
        ' - has a value
        ' - value is an integer
        ' - value is 0 or more

        Dim goalsScored As Int32
        Dim validInteger As Boolean = False

        If (Not String.IsNullOrWhiteSpace(RoundNumberTextBox.Text)) Then
            ' Round number has a value
            Try
                ' This will throw an exception if the value is not an integer
                goalsScored = Convert.ToInt32(GoalsScoredTextBox.Text)
                If (goalsScored < 0) Then
                    ' Goals scored is invalid so display message
                    ShowErrorMessage("Goals Scored cannot be a negative number")
                Else
                    validInteger = True
                End If
            Catch ex As Exception
                ShowErrorMessage("Goals Scored must be a number.")
            End Try

            If (Not validInteger) Then

                ' Remove value from text box as it is invalid
                Me.GoalsScoredTextBox.Text = ""

                ' Reset focus to text box so correct value can be entered
                ' before proceeding
                Me.GoalsScoredTextBox.Focus()
            End If
        End If
    End Sub

    Private Sub ShowErrorMessage(ByRef errorMessage)
        ' Display supplied message as an error message with an Error icon
        MessageBox.Show(errorMessage, "Robot Round", MessageBoxButtons.OK, MessageBoxIcon.Error)
    End Sub

    Private Sub CloseButton_Click(sender As Object, e As EventArgs) Handles CloseButton.Click
        CloseForm()
    End Sub

    Private Sub CloseForm()
        ' Close the round form, effectively stopping the program
        Me.Close()
    End Sub

    Public Sub ResetRound()
        ' Reset everything in this round so it's ready for a new round

        ' Recreate RobotRound object to clear all values
        RobotRound = New RobotRound()

        ' Reset all controls so that textboxes are blank and
        ' checkboxes are unchecked
        ResetControls(Me)

        ' Switch to Auto Mode tab, so it's ready for the next round
        Me.ModeTabControl.SelectedTab = Me.AutoTabPage
    End Sub

    Sub ResetControls(container As Control)
        ' For the control and it's nested controls reset all text boxes
        ' to empty strings and checkboxes to unchecked

        For Each control As Control In container.Controls
            If control.GetType() Is GetType(GroupBox) _
                Or control.GetType() Is GetType(TabControl) _
                Or control.GetType() Is GetType(TabPage) Then
                ' Group boxes, tab controls and tab pages have their own 
                ' controls so call method again with those nested controls
                ResetControls(control)
            End If
            If control.GetType Is GetType(TextBox) Then
                ' Remove text from this textbox
                control.Text = String.Empty
            ElseIf control.GetType Is GetType(CheckBox) Then
                ' Set this checkbox to be unchecked
                Dim checkbox As CheckBox = TryCast(control, CheckBox)
                checkbox.Checked = False
            End If
        Next

    End Sub

    Private Sub ResetRoundButton_Click(sender As Object, e As EventArgs) Handles ResetRoundButton.Click
        ResetRound()
    End Sub

    Private Sub ResetRoundToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ResetRoundToolStripMenuItem.Click
        ResetRound()
    End Sub

    Private Sub AutoReachedObstacleLabel_Click(sender As Object, e As EventArgs) Handles AutoReachedObstacleLabel.Click
        ' When label for Reached obstacle in Auto mode is clicked make it 
        ' behave like related checkbox is clicked
        SwitchCheckbox(Me.AutoReachedObstacleCheckBox)
    End Sub

    Private Sub AutoCrossedObstacleLabel_Click(sender As Object, e As EventArgs) Handles AutoCrossedObstacleLabel.Click
        ' When label for Crossed obstacle in Auto mode is clicked make it 
        ' behave like related checkbox is clicked
        SwitchCheckbox(Me.AutoCrossedObstacleCheckBox)
    End Sub

    Private Sub AutoScoredLowGoalLabel_Click(sender As Object, e As EventArgs) Handles AutoScoredLowGoalLabel.Click
        SwitchCheckbox(Me.AutoScoredLowGoalCheckBox)
    End Sub

    Private Sub AutoScoredHighGoalLabel_Click(sender As Object, e As EventArgs) Handles AutoScoredHighGoalLabel.Click
        ' When label for Scored High Goal in Auto mode is clicked make it 
        ' behave like related checkbox is clicked
        SwitchCheckbox(Me.AutoScoredHighGoalCheckBox)
    End Sub

    Private Sub TeleopRoughTerrainLabel_Click(sender As Object, e As EventArgs) Handles TeleopRoughTerrainLabel.Click
        ' When label for Rough Terrain in Teleop mode is clicked make it 
        ' behave like related checkbox is clicked
        SwitchCheckbox(Me.TeleopRoughTerrainCheckBox)
    End Sub

    Private Sub TeleopRockWallLabel_Click(sender As Object, e As EventArgs) Handles TeleopRockWallLabel.Click
        ' When label for Rock Wall in Teleop mode is clicked make it 
        ' behave like related checkbox is clicked
        SwitchCheckbox(Me.TeleopRockWallCheckBox)
    End Sub

    Private Sub TeleopSallyPortLabel_Click(sender As Object, e As EventArgs) Handles TeleopSallyPortLabel.Click
        ' When label for Sally Port in Teleop mode is clicked make it 
        ' behave like related checkbox is clicked
        SwitchCheckbox(Me.TeleopSallyPortCheckBox)
    End Sub

    Private Sub TeleopDrawbridgeLabel_Click(sender As Object, e As EventArgs) Handles TeleopDrawbridgeLabel.Click
        ' When label for Drawbridge in Teleop mode is clicked make it 
        ' behave like related checkbox is clicked
        SwitchCheckbox(Me.TeleopDrawbridgeCheckBox)
    End Sub

    Private Sub TeleopRampartsLabel_Click(sender As Object, e As EventArgs) Handles TeleopRampartsLabel.Click
        ' When label for Ramparts in Teleop mode is clicked make it 
        ' behave like related checkbox is clicked
        SwitchCheckbox(Me.TeleopRampartsCheckBox)
    End Sub

    Private Sub TeleopMoatLabel_Click(sender As Object, e As EventArgs) Handles TeleopMoatLabel.Click
        SwitchCheckbox(Me.TeleopMoatCheckBox)
    End Sub

    Private Sub TeleopChevalDeFrisLabel_Click(sender As Object, e As EventArgs) Handles TeleopChevalDeFrisLabel.Click
        ' When label for Cheval de Fris in Teleop mode is clicked make it 
        ' behave like related checkbox is clicked
        SwitchCheckbox(Me.TeleopChevalDeFrisCheckBox)
    End Sub

    Private Sub TeleopPortcullisLabel_Click(sender As Object, e As EventArgs) Handles TeleopPortcullisLabel.Click
        ' When label for Portcullis in Teleop mode is clicked make it 
        ' behave like related checkbox is clicked
        SwitchCheckbox(Me.TeleopPortcullisCheckBox)
    End Sub

    Private Sub TeleopLowBarLabel_Click(sender As Object, e As EventArgs) Handles TeleopLowBarLabel.Click
        ' When label for Low bar in Teleop mode is clicked make it 
        ' behave like related checkbox is clicked
        SwitchCheckbox(Me.TeleopLowBarCheckBox)
    End Sub

    Private Sub TeleopPickedUpBallsLabel_Click(sender As Object, e As EventArgs) Handles TeleopPickedUpBallsLabel.Click
        ' When label for Picked up balls in Teleop mode is clicked make it 
        ' behave like related checkbox is clicked
        SwitchCheckbox(Me.TeleopPickUpBallsCheckBox)
    End Sub

    Private Sub TeleopScoredLowGoalsLabel_Click(sender As Object, e As EventArgs) Handles TeleopScoredLowGoalsLabel.Click
        ' When label for Low Goals in Teleop mode is clicked make it 
        ' behave like related checkbox is clicked
        SwitchCheckbox(Me.TeleopScoreLowGoalsCheckBox)
    End Sub

    Private Sub TeleopScoredHighGoalsLabel_Click(sender As Object, e As EventArgs) Handles TeleopScoredHighGoalsLabel.Click
        ' When label for Scored high goals in Teleop mode is clicked make it 
        ' behave like related checkbox is clicked
        SwitchCheckbox(Me.TeleopScoreHighGoalsCheckBox)
    End Sub

    Private Sub TeleopClimbedWallLabel_Click(sender As Object, e As EventArgs) Handles TeleopClimbedWallLabel.Click
        ' When label for Climbed Walls in Teleop mode is clicked make it 
        ' behave like related checkbox is clicked
        SwitchCheckbox(Me.TeleopClimbWallCheckBox)
    End Sub

    Sub SwitchCheckbox(checkbox As CheckBox)
        ' Switch checked status of supplied checkbox
        ' If checked was true, set it to false
        ' If checked was false, set it to true
        checkbox.Checked = Not (checkbox.Checked)
    End Sub
End Class
