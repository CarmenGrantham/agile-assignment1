﻿Public Class RobotRound

    Public Property RoundId As Integer
    Public Property RobotId As String
    Public Property GoalsScored As Integer = 0


    ' List of autonomous mode elements
    Public Property AutoReachedObstacle As Boolean
    Public Property AutoCrossedObstacle As Boolean
    Public Property AutoScoredLowGoal As Boolean
    Public Property AutoScoredHighGoal As Boolean

    ' Robot past these obstacles in Teleop mode
    Public Property TeleopLowBar As Boolean
    Public Property TeleopPortcullis As Boolean
    Public Property TeleopChevalDeFris As Boolean
    Public Property TeleopMoat As Boolean
    Public Property TeleopRamparts As Boolean
    Public Property TeleopDrawbridge As Boolean
    Public Property TeleopSallyPort As Boolean
    Public Property TeleopRockWall As Boolean
    Public Property TeleopRoughTerrain As Boolean

    ' Robot demonstrated these abilities in Teleop mode
    Public Property TeleopPickUpBalls As Boolean
    Public Property TeleopScoreLowGoals As Boolean
    Public Property TeleopScoreHighGoals As Boolean
    Public Property TeleopClimbWall As Boolean

End Class
